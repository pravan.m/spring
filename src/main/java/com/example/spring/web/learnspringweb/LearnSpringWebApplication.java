package com.example.spring.web.learnspringweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnSpringWebApplication {

    public static void main(String[] args) {

        SpringApplication.run(LearnSpringWebApplication.class, args);

    }

}
